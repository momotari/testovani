package cz.cvut.fel;

import cz.cvut.fel.ts1.Momotari;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class MomotariTest {

    @Test
    public void factorialTest(){
        Momotari fac = new Momotari();

        assertEquals(1, fac.factorial(1));
        assertEquals(1, fac.factorial(0));
        assertEquals(120, fac.factorial(5));


    }
}
